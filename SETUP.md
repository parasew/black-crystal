# Dev Setup

## Linux

Requirements:
- arduino IDE (configured to work with esp32)
- pyserial
- git

Retrieve Arduino From their [software page](https://www.arduino.cc/en/Main/Software)

`wget https://downloads.arduino.cc/arduino-1.8.10-linux64.tar.xz`

Extract, review, and install

`tar xvf arduino-1.8.10-linux64.tar.xz`
`cd arduino-1.8.10; ./install.sh # after reviewing ./install.sh`

Add the additional boards:

`https://dl.espressif.com/dl/package_esp32_index.json`

Then go into `~/Arduino/libraries`

Then clone the m5stack repo:

`git clone git@github.com:m5stack/M5Stack.git`

**Tetris Test**

You should now be ready to Verify ('compile') and Upload ('flash') your first software.

You can use the gui to do this.
Open an `M5Stack` example `Files --> Open --> libraries --> examples --> Games --> Tetris` (yes, they have tetris)

**Working with Trezor Libs**

```
cd ~/Arduino
wget https://rmxwallet.io/files/m5stack.tar.gz
cd libraries/trezor-crypto
mv utility utility.identical.unused
mkdir unused
mv aescrypt.c aes_modes.c M5Display.* unused/
```

Open, Verify, and Upload `m5stack_trezor/m5stack_trezor.ino`

## Mac

## Windows






