# Black Crystal: Meta-Workshop for educational crypto workshops

The meta-workshop "Black Crystal" at 36C3 has the aim to investigate the trezor-crypto library as well as additional other parts (hardware, software and development environments) which can be used for educational and workshop purposes. In the previous workshops, hardware, software and an IDE has been identified to kickstart a very rudimentary 'workshop field kit', which will be improved upon. A specific aim is to pave the way for a 'Monero protoyping' environment, which should help to learn about the Monero codebase, and to understand the limitations of Monero on embedded systems.


# Order of the workshop

* Prototyping hardware for crypto-workflows
  - the ESP32 in detail (see [ESP32 Technical Reference Manual](https://www.espressif.com/sites/default/files/documentation/esp32_technical_reference_manual_en.pdf))
  - Limitations of slow MCUs for crypto , alternatives and workarounds
  - How to prototype: entropy sources, key generation and other basics
  - M5stack platform ([hackaday](https://hackaday.com/tag/m5stack/), [M5stack github](https://github.com/m5stack/M5Stack))
  - investigating I2C extensions (as entropy sources)
* Identify eduational material for basic functionality for cryptocurrency
    - Investigating [trezor-crypto library](https://github.com/trezor/trezor-crypto)
    - comparison to trezor monorepo ([trezor-firmware](https://github.com/trezor/trezor-firmware))
    - discussing alternatives to trezor-crypto
* Individual projects
  - development of [BIP39](https://iancoleman.io/bip39/)-based examples
  - improvements and extensions to the existing examples ([Shamir's Secret Sharing](https://github.com/fletcher/c-sss/blob/master/src/shamir.c))
  - creating Monero keys and key derivation
  - discussing other approaches, examples and workflows
* The road to a 'Monero protoyping' environment
  - colleting Monero-related missing pieces in trezor-crypto: bulletproofs, et.al.
* Monero on embedded systems: the current state of things
  - C/C++
  - Golang
  - Rust and Rust Embedded
  - Python
  - other options


## Setup Instructions

Goto [SETUP.md](./SETUP.md)
